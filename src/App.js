import React, { useState } from 'react';
import './App.css';
import CancelButton from './components/CancelButton'

const App = () => {
  const [dataSuccess, setSuccess] = useState({})
  const [status, setStatus] = useState('pending')
  const os = "ios"

  window.actions = {
    stuffWasSuccesful: (response) => {
      setStatus("OK");
      setSuccess({...JSON.parse(response)});
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <CancelButton os={os}/>
        <p>{ dataSuccess.title }</p>
        <p>{ status }</p>
      </header>
    </div>
  )
}

export default App;
