import React from 'react'

const CancelButton = ({ os }) => {
  return(
    <button className="btn" onClick={() => { cancel(os) }}>Cancelar</button>
  )
}
const { Android, webkit } = window;

const OS = {
  android: (typeof Android != "undefined") && Android,
  ios: (typeof webkit != "undefined") && webkit
}

const cancel = (os) => {
  console.log(OS[`${os}`])
  if (!OS[`${os}`]) {
    return false
  }

  switch(os) {
    case 'ios':
      OS[`${os}`].messageHandlers.onCancel.postMessage("Cancelar pago")
      break;
    case 'android':
      OS[`${os}`].onClose();
      break;
    default:
      console.log("An origin it was't send")
  }
}

export default CancelButton
